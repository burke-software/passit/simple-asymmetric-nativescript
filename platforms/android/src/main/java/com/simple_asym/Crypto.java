package com.simple_asym;


import android.support.annotation.NonNull;
import android.util.Base64;
import java.util.Arrays;

import org.libsodium.jni.NaCl;
import org.libsodium.jni.Sodium;


class KeyPair {
	byte[] publicKey;
	byte[] privateKey;

	public KeyPair(byte[] public_key, byte[] private_key) {
		this.publicKey = public_key;
		this.privateKey = private_key;
	}
}


public class Crypto {
	private static final int BASE64_FLAGS = Base64.NO_WRAP | Base64.NO_CLOSE;

	public static byte[] fromBase64(String input) {
		byte[] inputBytes = Base64.decode(input, BASE64_FLAGS);
		return inputBytes;
	}

	public static String toBase64(String input) {
		String out = Base64.encodeToString(input.getBytes(), BASE64_FLAGS);
		return out;
	}

	public static String toBase64(byte[] input) {
		String out = Base64.encodeToString(input, BASE64_FLAGS);
		return out;
	}

	public static KeyPair generateKeys() {
		Sodium sodium= NaCl.sodium();
		long publickeylen=Sodium.crypto_box_publickeybytes();
		long privatekeylen=Sodium.crypto_box_secretkeybytes();
		final byte[] public_key=new byte[(int)publickeylen];
		final byte[] private_key=new byte[(int)privatekeylen];
		Sodium.crypto_box_keypair(public_key, private_key);
		KeyPair keyPair = new KeyPair(public_key, private_key);
		return keyPair;
	}

	private static byte[] makeSalt() {
		Sodium sodium= NaCl.sodium();
		byte[] salt = new byte[Sodium.crypto_pwhash_saltbytes()];
		Sodium.randombytes(salt, salt.length);
		return salt;
	}

	public static byte[] hashPassword(String password, byte[] salt) {
		Sodium sodium= NaCl.sodium();
		byte[] key = new byte[Sodium.crypto_box_seedbytes()];
		byte[] passwd = password.getBytes();
		Sodium.crypto_pwhash(
			key,
			key.length,
			passwd,
			passwd.length,
			salt,
			Sodium.crypto_pwhash_opslimit_interactive(),
			Sodium.crypto_pwhash_memlimit_interactive(),
			Sodium.crypto_pwhash_alg_default()
		);
		return key;
	};

	public static byte[] hashPassword(byte[] password, byte[] salt) {
		Sodium sodium= NaCl.sodium();
		byte[] key = new byte[Sodium.crypto_box_seedbytes()];
		byte[] passwd = password;
		Sodium.crypto_pwhash(
			key,
			key.length,
			passwd,
			passwd.length,
			salt,
			Sodium.crypto_pwhash_opslimit_interactive(),
			Sodium.crypto_pwhash_memlimit_interactive(),
			Sodium.crypto_pwhash_alg_default()
		);
		return key;
	};

	// Trim 0 bytes from end of byte array
    private static byte[] trim(byte[] bytes)
    {
        int i = bytes.length - 1;
        while (i >= 0 && bytes[i] == 0)
        {
            --i;
        }

        return Arrays.copyOf(bytes, i + 1);
    }

	public static String exportPublicKey(KeyPair keyPair) {
		String pub_key_base64 = Base64.encodeToString(keyPair.publicKey, BASE64_FLAGS);
		return pub_key_base64;
	}

	public static String exportPrivateKey(KeyPair keyPair) {
		String pri_key_base64 = Base64.encodeToString(keyPair.privateKey, BASE64_FLAGS);
		return pri_key_base64;
	}

	public static String exportPrivateKey(KeyPair keyPair, String password) {
		byte[] salt = makeSalt();
		byte[] hash = hashPassword(password, salt);
		byte[] ciphertext = encrypt(hash, keyPair.privateKey);
		byte[] combinedBuffer = new byte[salt.length + ciphertext.length];
		System.arraycopy(salt, 0, combinedBuffer, 0, salt.length);
		System.arraycopy(ciphertext, 0, combinedBuffer, salt.length, ciphertext.length);
		String privateKeyBase64 = Base64.encodeToString(combinedBuffer, BASE64_FLAGS);
		return privateKeyBase64;
	}

	public static KeyPair importKeyPair(String public_key, String private_key) {
		byte[] public_key_arr = Base64.decode(public_key, BASE64_FLAGS);
		byte[] private_key_arr = Base64.decode(private_key, BASE64_FLAGS);

		KeyPair keyPair = new KeyPair(public_key_arr, private_key_arr);
		return keyPair;
	}

	public static KeyPair importKeyPair(String public_key, String private_key, String password) {
		Sodium sodium= NaCl.sodium();
		byte[] pk = Base64.decode(private_key, BASE64_FLAGS);

		byte[] salt = Arrays.copyOfRange(pk, 0, Sodium.crypto_pwhash_saltbytes());
		byte[] ciphertext = Arrays.copyOfRange(pk, Sodium.crypto_pwhash_saltbytes(), pk.length);
		byte[] hash = hashPassword(password, salt);
		byte[] priv = decrypt(hash, ciphertext);

		byte[] public_key_arr = Base64.decode(public_key, BASE64_FLAGS);

		KeyPair keyPair = new KeyPair(public_key_arr, priv);
		return keyPair;
	}

	public static byte[] encrypt(byte[] key, byte[] message) {
		Sodium sodium= NaCl.sodium();
		byte[] nonce = new byte[Sodium.crypto_secretbox_noncebytes()];
		Sodium.randombytes(nonce, nonce.length);
		byte[] ciphertext = new byte[Sodium.crypto_secretbox_macbytes() + message.length];

		Sodium.crypto_secretbox_easy(ciphertext, message, message.length, nonce, key);

		byte[] result = new byte[nonce.length + ciphertext.length];
		System.arraycopy(nonce, 0, result, 0, nonce.length);
		System.arraycopy(ciphertext, 0, result, nonce.length, ciphertext.length);
		return result;
	}

	public static String encrypt(String key, String message) {
		byte[] keyBytes = Base64.decode(key, BASE64_FLAGS);
		byte[] messageBytes = message.getBytes();
		byte[] ciphertext = encrypt(keyBytes, messageBytes);
		String out = Base64.encodeToString(ciphertext, BASE64_FLAGS);
		return out;
	}

	public static String encrypt(byte[] key, String message) {
		byte[] messageBytes = message.getBytes();
		byte[] ciphertext = encrypt(key, messageBytes);
		String out = Base64.encodeToString(ciphertext, BASE64_FLAGS);
		return out;
	}

	public static byte[] decrypt(byte[] key, byte[] ciphertext) {
		byte[] nonce = Arrays.copyOfRange(ciphertext, 0, Sodium.crypto_secretbox_noncebytes());
		byte[] encryptedMessage = Arrays.copyOfRange(ciphertext, Sodium.crypto_secretbox_noncebytes(), ciphertext.length);
		// We don't know the original message length, but it must be smaller than the ciphertext.
		byte[] decrypted = new byte[encryptedMessage.length];
		Sodium.crypto_secretbox_open_easy(decrypted, encryptedMessage, encryptedMessage.length, nonce, key);
		// Now trim any remaining 0 bytes
		decrypted = trim(decrypted);
		return decrypted;
	}

	public static String decrypt(String key, String ciphertext) {
		byte[] keyBytes = Base64.decode(key, BASE64_FLAGS);
		byte[] ciphertextBytes = Base64.decode(ciphertext, BASE64_FLAGS);
		byte[] plainBytes = decrypt(keyBytes, ciphertextBytes);
        String out = new String(plainBytes);
		return out;
	}

	public static String decrypt(byte[] key, String ciphertext) {
		byte[] ciphertextBytes = Base64.decode(ciphertext, BASE64_FLAGS);
		byte[] plainBytes = decrypt(key, ciphertextBytes);
        String out = new String(plainBytes);
		return out;
	}

	public static byte[] generateSymmetricKey() {
		Sodium sodium= NaCl.sodium();
        byte[] key = new byte[Sodium.crypto_box_publickeybytes()];
		Sodium.randombytes_buf(key, key.length);
		return key;
	}

	public static byte[] rsaEncrypt(byte[] theirPublicKey, byte[] plaintext) {
        Sodium sodium= NaCl.sodium();
        byte[] ciphertext=new byte[Sodium.crypto_box_sealbytes() + plaintext.length];
        Sodium.crypto_box_seal(ciphertext, plaintext, plaintext.length, theirPublicKey);
        return ciphertext;
    }

    public static String rsaEncrypt(String theirPublicKey, String plaintext) {
        byte[] publicKeyBytes = Base64.decode(theirPublicKey, BASE64_FLAGS);
        byte[] messageBytes = plaintext.getBytes();
        byte[] encrypted = rsaEncrypt(publicKeyBytes, messageBytes);
        String out = Base64.encodeToString(encrypted, BASE64_FLAGS);
        return out;
    }

    public static byte[] rsaDecrypt(byte[] myPublicKey, byte[] myPrivateKey, byte[] ciphertext) {
        Sodium sodium = NaCl.sodium();
        byte[] plaintext = new byte[ciphertext.length];
        Sodium.crypto_box_seal_open(plaintext, ciphertext, ciphertext.length, myPublicKey, myPrivateKey);
        plaintext = trim(plaintext);
        return plaintext;
    }

    public static String rsaDecrypt(String myPublicKey, String myPrivateKey, String ciphertext) {
        byte[] myPublicKeyBytes = Base64.decode(myPublicKey, BASE64_FLAGS);
        byte[] myPrivateKeyBytes = Base64.decode(myPrivateKey, BASE64_FLAGS);
        byte[] ciphertextBytes = Base64.decode(ciphertext, BASE64_FLAGS);
        byte[] decrypted = rsaDecrypt(myPublicKeyBytes, myPrivateKeyBytes, ciphertextBytes);
        String out = new String(decrypted);
        return out;
    }
}

