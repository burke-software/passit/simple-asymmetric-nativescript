interface KeyPair {
	publicKey: Uint8Array;
	privateKey: Uint8Array;
}

declare module com {
	export module simple_asym {
		export class Crypto {
			public static fromBase64(input: string): Uint8Array;
			public static toBase64(input: string | Uint8Array): string;
			public static generateKeys(): KeyPair;
			public static exportPublicKey(keyPair: KeyPair): string;
			public static exportPrivateKey(keyPair: KeyPair, password?: string): string;
			public static importKeyPair(public_key: string, private_key: string, password?: string): KeyPair;
			public static hashPassword(password: string, salt: Uint8Array): Uint8Array;
			public static makeSalt(): Uint8Array;
			public static rsaEncrypt(theirPublicKey: string, plaintext: string): string;
			public static rsaEncrypt(theirPublicKey: Uint8Array, plaintext: Uint8Array): Uint8Array;
			public static rsaDecrypt(myPublicKey: string, myPrivateKey: string, ciphertext: string): string;
			public static rsaDecrypt(myPublicKey: Uint8Array, myPrivateKey: Uint8Array, ciphertext: Uint8Array): Uint8Array;
			public static generateSymmetricKey(): Uint8Array;
			public static encrypt(key: Uint8Array, message: Uint8Array): Uint8Array;
			public static encrypt(key: string, message: string): string;
			public static encrypt(key: Uint8Array, message: string): string;
			public static decrypt(key: string, message: string): string;
			public static decrypt(key: Uint8Array, message: Uint8Array): Uint8Array;
			public static decrypt(key: Uint8Array, message: string): string;
		}
	}
}