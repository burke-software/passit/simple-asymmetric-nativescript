let JavaCrypto = com.simple_asym.Crypto;

let NaCl = org.libsodium.jni.NaCl;
let Sodium = org.libsodium.jni.Sodium;
let Base64 = android.util.Base64;

let BASE64_SAFE_URL_FLAGS = 0;

declare const Buffer;

export const generateKeys = () => {
	return JavaCrypto.generateKeys();
};

/**
 * Trim off new lines and whitespace that might have been added by java implimentations
 */
export const trimJavaString = (input: string): string => {
	return input.replace(/(\r\n|\n|\r)/gm,"").trim();
};

export const exportPublicKey = (keyPair: KeyPair) => {
	let key = JavaCrypto.exportPublicKey(keyPair);
	return trimJavaString(key);
};

export const exportPrivateKey = (keyPair: KeyPair, password?: string): string => {
	let key: string;
	if (password) {
		key = JavaCrypto.exportPrivateKey(keyPair, password);
	} else {
		key = JavaCrypto.exportPrivateKey(keyPair);
	}
	return trimJavaString(key);
};

export const importKeyPair = (public_key: string, private_key: string, password?: string) => {
	if (password) {
		let keyPair = JavaCrypto.importKeyPair(public_key, private_key, password);
		return keyPair;
	}
	return JavaCrypto.importKeyPair(public_key, private_key);
}

/**
 * Use js version because java port doesn't support pwhash
 */
export const hashPassword = (password: string, salt: string | Uint8Array): Uint8Array => {
	if (typeof(salt) === 'string') {
		salt = JavaCrypto.fromBase64(salt);
	}
	// Convert byte array to js array to support nativescript java marshalling
	// It will still work this way - just need to get the byte ints into java..
	let hash = JavaCrypto.hashPassword(password, [].slice.call((salt)));
	return hash;
};

export const makeSalt = (): Uint8Array => {
	return JavaCrypto.makeSalt();
};

export const encrypt = (key: string | Uint8Array, message: string, base64 = true) => {
	if (base64 === false) {
		console.error('not implemented');
	}
	return JavaCrypto.encrypt(key as any, message);
};

export const decrypt = (key: string | Uint8Array, ciphertext: string, base64 = true) => {
	return JavaCrypto.decrypt(key as any, ciphertext);
};

export function generateSymmetricKey(useBase64?: boolean): Uint8Array {
	let key = JavaCrypto.generateSymmetricKey();
	return key;
};

export const rsaEncrypt = (theirPublicKey: string, plaintext: string, useBase64=true): string => {
	return JavaCrypto.rsaEncrypt(theirPublicKey, plaintext);
};

export const rsaDecrypt = (myPublicKey: string, myPrivateKey: string, ciphertext: string): string => {
	return JavaCrypto.rsaDecrypt(myPublicKey, myPrivateKey, ciphertext);
};

export const toBase64 = (input: string | Uint8Array) => {
	return JavaCrypto.toBase64(input);
};

export const fromBase64 = (input: string) => {
	return JavaCrypto.fromBase64(input);
};