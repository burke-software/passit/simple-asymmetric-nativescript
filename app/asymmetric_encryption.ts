import { MissingSymmetricKeyException, MissingRsaKeyPairException } from './exceptions';
import {
    toBase64,
    fromBase64,
    generateKeys,
    exportPrivateKey,
    exportPublicKey,
    importKeyPair,
    generateSymmetricKey,
    rsaEncrypt,
    rsaDecrypt,
    encrypt,
    decrypt,
} from './crypto';

interface IKeyPair extends KeyPair {};

export default class Asym {
    private keyPair: IKeyPair;
    private symmetricKey: Uint8Array;

    /** Makes new private key and sets for internal usage
     * Returns [privateKey, publicKey] as strings. If password is set,
     * the private key will be encrypted and ciphertext will be returned
     */
    public makeRsaKeys(password?: string): [string, string] {
        this.keyPair = generateKeys();
        let publicKey = exportPublicKey(this.keyPair);
        let privateKey = exportPrivateKey(this.keyPair, password);
        return [privateKey, publicKey];
    }

    /** Import a public and private key pair for Asym object usage (RSA encryption and decryption)
     */
    public setKeyPair(publicKey: string, privateKey: string, password?: string): IKeyPair {
        this.keyPair = importKeyPair(publicKey, privateKey, password);
        return this.keyPair;
    }

    /**
     * Get base64 version of public key
     */
    public getPublicKey(): string {
        return exportPublicKey(this.keyPair);
    }

    /**
     * Return symmetric key in plain (unecrypted) base64 format
     */
    public getSymmetricKey(): string {
        return toBase64(this.symmetricKey);
    }

    /**
     * Perform RSA Encryption (asymmetric)
     * @param publicKey "Their" public key we want to encrypt something for
     * @param plaintext plaintext string to encrypt
     * @param use_base64 
     */
    public rsaEncrypt(publicKey: string, plaintext: string | Uint8Array, use_base64 = true): string | Uint8Array {
        if (typeof(plaintext) !== 'string') {
            plaintext = toBase64(plaintext);
        }
        return rsaEncrypt(publicKey, plaintext, use_base64);
    }

    /**
     * Perform RSA Decryption (asymmetric)
     * @param ciphertext ciphertext string to decrypt
     * @param use_base64 
     */
    public rsaDecrypt(ciphertext: string, use_base64 = true): string {
        let public_key = exportPublicKey(this.keyPair); 
        let private_key = exportPrivateKey(this.keyPair);
        return rsaDecrypt(public_key, private_key, ciphertext);
    }

    /**
     * Generate random symmetric key for internal usage and return base64 string of it 
     */
    public makeSymmetricKey(): string {
        this.symmetricKey = generateSymmetricKey();
        return toBase64(this.symmetricKey);
    }


    /**
     * Set asym symmetric key (used for secret key encryption)
     * @param key base64 or binary key
     */
    public setSymmetricKey(key: string | Uint8Array): void {
        if (typeof(key) === 'string') {
            this.symmetricKey = fromBase64(key);
        } else {
            this.symmetricKey = key;
        }
    }

    /**
     * Get encrypted version of symmetric key to share
     * @param public_key "Their" public key
     */
    public getEncryptedSymmetricKey(publicKey: string): string {
        return this.rsaEncrypt(publicKey, this.symmetricKey) as string;
    }

    public setSymmetricKeyFromEncrypted(ciphertext: string): void {
        let decryptedKey = this.rsaDecrypt(ciphertext);
        return this.setSymmetricKey(decryptedKey);
    }

    public encrypt(plaintext: string): string {
        if (!this.symmetricKey) {
            throw MissingSymmetricKeyException;
        }
        return encrypt(this.symmetricKey, plaintext);
    }

    public decrypt(ciphertext: string): string {
        if (!this.symmetricKey) {
            throw MissingSymmetricKeyException;
        }
        return decrypt(this.symmetricKey, ciphertext);
    }
}