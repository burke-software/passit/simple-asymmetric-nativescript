import Asym from '../asymmetric_encryption';

describe('Asym class can', () => {
    let asym: Asym;

    beforeEach(function(): void {
        asym = new Asym();
    });

    it('make RSA keys and ensure they are a string that can be saved', () => {
        let keys = asym.makeRsaKeys();
        expect(keys[0].length).toBe(44);
        expect(keys[1].length).toBe(44);
        expect(typeof(keys[0])).toBe('string');
    });

    it('make RSA key with password', () => {
        let keys = asym.makeRsaKeys('123456');
        expect(keys[0].length).toBe(120);
        expect(keys[1].length).toBe(44);
        expect(typeof(keys[0])).toBe('string');
    });

    it('import RSA key pair', () => {
        let privateKeyBase64 = 'TCkVmbGlDxi9t+ZIYeYTgRLoHCe7kmW4AhfS8/VKqkI=';
        let publicKeyBase64 = 'wL+JHgcJs5aRYMjF8QmHcUVCWdE5ENzLVsKEf9V2UHM=';
        let keyPair = asym.setKeyPair(publicKeyBase64, privateKeyBase64);
        expect(keyPair).toBeTruthy();
    });

    it('import RSA key pair with encrypted private key', () => {
        let privateKeyBase64 = 'HNP8i6zQS6Kj1b0tovnnySFCfLHWJ7ZBKfHCqperfAbgRXdlabRgRtTbb2ZyT25isGkg390tKGn+rE7emNgbkqvf9Qd6CgG37pclw0UUQy9TruasDA3/lQ==';
        let publicKeyBase64 = 'KJ6WVf8VsH6D9842K+j0o8kGapyGSuT+MYwiw8MTdCY=';
        let password = '123456';

        let keyPair = asym.setKeyPair(publicKeyBase64, privateKeyBase64, password);
        expect(asym.getPublicKey().length).toBe(44);
        expect(keyPair).toBeTruthy();
    });

    it('encrypt and decrypt using RSA public key', () => {
        let plaintext = 'hi☢⛴';

        let privateKeyBase64 = 'TCkVmbGlDxi9t+ZIYeYTgRLoHCe7kmW4AhfS8/VKqkI=';
        let publicKeyBase64 = 'wL+JHgcJs5aRYMjF8QmHcUVCWdE5ENzLVsKEf9V2UHM=';
        asym.setKeyPair(publicKeyBase64, privateKeyBase64);
        
        let ciphertext = asym.rsaEncrypt(asym.getPublicKey(), plaintext) as string;
        expect(ciphertext).not.toEqual(plaintext);
        let decrypted = asym.rsaDecrypt(ciphertext);
        expect(decrypted).toBe(plaintext);
    });

    it('set symmetric key', () => {
        let key = asym.makeSymmetricKey();
        asym.setSymmetricKey(key);
        expect((asym as any).symmetricKey).toBeDefined();
    });

    it('get and set an encrypted shared symmetric key', () => {
        let privateKeyBase64 = 'TCkVmbGlDxi9t+ZIYeYTgRLoHCe7kmW4AhfS8/VKqkI=';
        let publicKeyBase64 = 'wL+JHgcJs5aRYMjF8QmHcUVCWdE5ENzLVsKEf9V2UHM=';
        asym.setKeyPair(publicKeyBase64, privateKeyBase64);
        let key = asym.makeSymmetricKey();
        let encryptedKey = asym.getEncryptedSymmetricKey(asym.getPublicKey());
        expect(encryptedKey).not.toEqual(key);
        asym.setSymmetricKeyFromEncrypted(encryptedKey);
        expect(asym.getSymmetricKey()).toEqual(key);
    });

    it('encrypt and decrypt using symmetric key', () => {
        let key = 'HMS11YymAKX5z6d6/hhdvyGtj7wiTfwzaO0O3ptSHZ4=';
        let message = 'hello';
        asym.setSymmetricKey(key);
        let ciphertext = asym.encrypt(message);
        let decrypted = asym.decrypt(ciphertext);
        expect(decrypted).toBe(message);
    });

    it('bob and alice integration test', () => {
        let bob = new Asym();
        let alice = new Asym();
        let message = 'hello';

        // Generate both keypairs
        bob.makeRsaKeys();
        alice.makeRsaKeys();

        // Generate one (to be shared) symmetric key
        bob.makeSymmetricKey();

        // Key exchange
        let encryptedSymmetricKey = bob.getEncryptedSymmetricKey(alice.getPublicKey());
        alice.setSymmetricKeyFromEncrypted(encryptedSymmetricKey);

        // Send a message
        let ciphertext = bob.encrypt(message);
        expect(ciphertext).not.toEqual(message);
        // It should be base64 (not binary)
        expect(typeof(ciphertext)).toEqual('string');
        let decryptedMessage = alice.decrypt(ciphertext);
        expect(decryptedMessage).toEqual(message);
    });
});
