import { 
	generateKeys,
	importKeyPair,
	exportPrivateKey,
	exportPublicKey,
	hashPassword,
	encrypt,
	decrypt,
	rsaEncrypt,
	rsaDecrypt,
	makeSalt,
} from "../crypto";

let NaCl = org.libsodium.jni.NaCl;
let Sodium = org.libsodium.jni.Sodium;

var Buffer = require('buffer/').Buffer;

describe("crypto can", function() {
	it('make and export RSA key', () => {
		let keys = generateKeys();
		let publicKey = exportPublicKey(keys);
		let privateKey = exportPrivateKey(keys);
		expect(publicKey.length).toBe(44);
		expect(privateKey.length).toBe(44);
	});

	it('import a RSA key from self', () => {
		let keys = generateKeys();
		let privateKey = exportPrivateKey(keys);
		let publicKey = exportPublicKey(keys);
		let importedKeyPair = importKeyPair(publicKey, privateKey);
		let importedPrivateKey = exportPrivateKey(importedKeyPair);
		expect(importedPrivateKey).toEqual(privateKey);
	});

	it('import a RSA key from base64', () => {
		let privateB64 = '2A28PjsDW/WpQqMXPLTIkL99VwnofGRqNL7wdJrzUyo=';
		let publicB64 = 'jim48h9bcjw85PcnaSjOAvg022kJ/Q4aTzxb8hPtGlE=';
		let importedKeyPair = importKeyPair(publicB64, privateB64);
	});

	it('export and import private key with password', () => {
		let privateB64 = '2A28PjsDW/WpQqMXPLTIkL99VwnofGRqNL7wdJrzUyo=';
		let publicB64 = 'jim48h9bcjw85PcnaSjOAvg022kJ/Q4aTzxb8hPtGlE=';
		let password = 'hunter2';
		let keyPair = importKeyPair(publicB64, privateB64);
		let exportedPublicKey = exportPublicKey(keyPair);
		expect(exportedPublicKey).toEqual('jim48h9bcjw85PcnaSjOAvg022kJ/Q4aTzxb8hPtGlE=');
		let exportedPrivateKey = exportPrivateKey(keyPair, password);
		expect(exportedPrivateKey.length).toBe(120);
		let importedKey = importKeyPair(exportedPublicKey, exportedPrivateKey, password);

		let originalPrivateKey = exportPrivateKey(keyPair);
		let importedPrivateKey = exportPrivateKey(importedKey);
		expect(importedPrivateKey).toEqual(originalPrivateKey);
	});

	it('hashes password', () => {
		let password = 'hunter2';
		let salt = new Uint8Array([88, 240, 185, 66, 195, 101, 160, 138, 137, 78, 1, 2, 3, 4, 5, 6]);
		let hash = hashPassword(password, salt);
		expect(hash[0]).toEqual(49);
	});

	it('export with password', () => {
	  let password = 'hunter2';
	  let keys = generateKeys();
	  let privateKey = exportPrivateKey(keys, password);
	  expect(privateKey).toBeTruthy();
	});

	it('RSA Encrypt and Decrypt', () => {
		let keys = generateKeys();
		let message = 'test Ơ';
		let publicKey = exportPublicKey(keys);
		let privateKey = exportPrivateKey(keys);
		let ciphertext = rsaEncrypt(publicKey, message);
		expect(ciphertext).toBeTruthy();
		expect(ciphertext).not.toEqual(message);
		let decryptedMessage = rsaDecrypt(publicKey, privateKey, ciphertext);
		expect(decryptedMessage).toEqual(message);
	});

	it('Encrypt and decrypt text', () => {
		let key = Buffer.from('aaaabbccc3cccccccccb092810ec86d7e35c9d067702b31ef90bc43a7b598626749914d6a3e033ed', 'hex');
		let keyb64: string = key.toString('base64')
		let message = 'hellƠ';
		let ciphertext = encrypt(keyb64, message);
		expect(ciphertext).not.toEqual(message);
		let decryptedMessage = decrypt(keyb64, ciphertext);
		expect(decryptedMessage).toBe(message);
	});
});
