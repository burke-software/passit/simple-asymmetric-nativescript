/**
 * Error for missing aes key
 * @type {Error}
 */
export let MissingSymmetricKeyException: Error = new Error('Missing Symmetric key. Set or generate one');

/**
 * Error for missing public key
 * @type {Error}
 */
export let MissingRsaKeyPairException: Error = new Error('Missing RSA key pair. Set or generate one to use RSA encryption');